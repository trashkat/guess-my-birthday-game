from random import randint

#have computer ask users name

name = input("Hi! What is your name? ")


#have computer guess up to 5 times your birth month and year
#give the computer a range with variables that can be modified
#in order to help narrow down the range

low_month = 1
high_month = 12

low_year = 1924
high_year = 2004

guesses = []

for num in range(5):
    month_guess = randint(low_month,high_month)
    year_guess = randint(low_year,high_year)
    print("Guess ",num+1, ":", name, "were you born in", month_guess, "/", year_guess, "?" )
    response = input("How'd I do?  ")
    if response == "yes":                               #needs to be the first condition
       print("I knew it!")
       break
    # elif num+1 == 5:
    #     print("I have other things to do. Adiosss...!")
    elif response == "earlier":
        high_year = year_guess - 1
        guesses.append([year_guess, "before you were born."])
        print("Drat! Lemme try again!")
    elif response == "later":
        low_year = year_guess + 1
        guesses.append([year_guess, "after you were born."])
        print("Drat! Lemme try again!")
    else:
        response = input("That doesn't help me...earlier or later?  ")

print("I made", len(guesses), "guesses: ")

for guess, result in guesses:
    print("I guessed,", guess, "which was", result)

if len(guesses) == 5:
    print("You won!")
else:
    print("I won!!")
